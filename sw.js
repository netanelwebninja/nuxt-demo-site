importScripts('/_nuxt/workbox.4c4f5ca6.js')

workbox.precaching.precacheAndRoute([
  {
    "url": "/_nuxt/1db4d5fe98b447ceb32b.js",
    "revision": "c9cd13abcdb993e5c1f6503f84b27cc6"
  },
  {
    "url": "/_nuxt/20aa0ca1dc5a66e639e0.js",
    "revision": "d9027eefdf13890378e3fafbe1d15bc7"
  },
  {
    "url": "/_nuxt/20ad845bd72915fdd259.js",
    "revision": "b5a21352015bc6792d38b4dccf7dd9f5"
  },
  {
    "url": "/_nuxt/398f2cd4fc3e11cf6406.js",
    "revision": "2e0ccbf120538d4a84fa202d1714f8d4"
  },
  {
    "url": "/_nuxt/3c62388a3089dcda670c.js",
    "revision": "372b1e6368d118248399812820bc61f1"
  },
  {
    "url": "/_nuxt/42eb9275236c0dee52f6.js",
    "revision": "63236c1043b6231f07b151cdf1617f44"
  },
  {
    "url": "/_nuxt/4c33283c70239f55bbd6.css",
    "revision": "2c29fe9a9a1d82e51ca149000703c7d3"
  },
  {
    "url": "/_nuxt/6b5ed33ca8342742a617.js",
    "revision": "00cad1e5350ff7cfde31b03e7914dc1c"
  },
  {
    "url": "/_nuxt/9ba87c9d5e57c62a0363.js",
    "revision": "4ec6fed7fc2bc050d7130a9d2fd9d14b"
  },
  {
    "url": "/_nuxt/a59b5f9cfd5418657958.js",
    "revision": "8a9032b34a5ddbfb3f1033639a44134f"
  },
  {
    "url": "/_nuxt/a7de0514aaac605433e0.js",
    "revision": "dadb9ca4fe01c5e41a1623047c30d69b"
  },
  {
    "url": "/_nuxt/a88403fe5e40be86959b.js",
    "revision": "d0eadc8865b2b39dcd1438df674ddd26"
  },
  {
    "url": "/_nuxt/bedc8c144361463136e2.js",
    "revision": "35368a07ce71ca1f10ca2a9937363fe6"
  },
  {
    "url": "/_nuxt/c2975bae7c410811f449.js",
    "revision": "15c8b6fb5a6c5f3172bebe86ca9cb945"
  },
  {
    "url": "/_nuxt/cadd2e1af93c6e2ccc6e.js",
    "revision": "f3e5e4f8dc7ebf1e9a47a5c298d0e391"
  },
  {
    "url": "/_nuxt/cbc0fa8d89a30c09e4bb.css",
    "revision": "17cf9ee81eac3a8f7d4ad401ac5c8c42"
  },
  {
    "url": "/_nuxt/cf09ba0353d3d91f3ca4.js",
    "revision": "68a638e41db5ce3f162e425980c2c03b"
  },
  {
    "url": "/_nuxt/e08540e18922ffc43d00.js",
    "revision": "bb413e786a3362b800a1249fe2c30e33"
  }
], {
  "cacheId": "getpacking-website",
  "directoryIndex": "/",
  "cleanUrls": false
})

workbox.clientsClaim()
workbox.skipWaiting()

workbox.routing.registerRoute(new RegExp('/_nuxt/.*'), workbox.strategies.cacheFirst({}), 'GET')

workbox.routing.registerRoute(new RegExp('/.*'), workbox.strategies.networkFirst({}), 'GET')
